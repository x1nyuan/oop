#ifndef CLASS
#include <stdbool.h>
#include <stddef.h>

typedef struct{
    bool M0;
    bool M1;
    bool M2;
}State;
typedef struct{
    bool X0;
    bool X1;
    bool X2;
}Input;

typedef struct{
    bool state[3][2];
    bool input[3][2];
}Scon;

void __init__(Scon* this);
void setTestInput(Input* input, int pattern);
void scan(Scon* this, Input input);
void viewState(Scon* this);
void viewInput(Scon* this);

#endif
