#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Staff.h"
#include "ladder.h"

static void end_proc(Scon* this){
    int n, i;
    n = sizeof(this->state)/sizeof(bool)/2;
    for(i=0;i<n;i++){
        this->state[i][1] = this->state[i][0];
    }
    n = sizeof(this->input)/sizeof(bool)/2;
    for(i=0;i<n;i++){
        this->input[i][1] = this->input[i][0];
    }
}
/*  */

void setTestInput(Input* input, int pattern){
    int i, n;
    n = sizeof(*input);
    bool* bPattern;
    bPattern = (bool *)malloc(sizeof(bool)*n);
    for(i=0;i<n;i++){
        bPattern[i] = (pattern&(1<<i));
    }
    memcpy(input, bPattern, sizeof(*input));
}
/*  */
static void sampling(Scon* this, Input input){
    int n, i;
    bool* init_bool;
    n = sizeof(input)/sizeof(bool);

    printf("%d bits have been initalized for input\n", n);
    init_bool = (bool *)malloc(sizeof(bool)*n);
    memcpy(init_bool, &input,sizeof(input));

    for(i=0;i<n;i++){
        this->input[i][0] = init_bool[i];
    }
}
/*  */
void __init__(Scon* this){
    int n, i;
    bool* init_bool;
    n = sizeof(*this)/sizeof(bool);
    printf("--------This is init---------\n");
    printf("%d bits have been initalized for all\n", n);

    init_bool = (bool *)malloc(sizeof(bool)*n);

    for(i=0;i<n;i++){
        init_bool[i] = 0;
    }

    memcpy(this, init_bool,sizeof(*this));

    end_proc(this);
}
/*  */
void scan(Scon* this, Input input){
    printf("--------This is scan---------\n");
    sampling(this, input);

    if(riseSig(this->input[0])){
        this->state[2][0] = 1;
    }
    if(fallSig(this->input[1])){
        this->state[2][0] = 0;
    }

    end_proc(this);

}
/*  */
void viewState(Scon* this){
    int n, i;
    n = sizeof(this->state)/sizeof(bool)/2;
    for(i=0;i<n;i++){
        printf("flag%d is %d\n", i, this->state[i][0]);
    }
}
void viewInput(Scon* this){
    int n, i;
    n = sizeof(this->input)/sizeof(bool)/2;
    for(i=0;i<n;i++){
        printf("input%d is %d\n", i, this->input[i][0]);
    }
}
