import numpy as np
import itertools
from m2l import Mnemo


class LadderSim(object):
    def __init__(self, inDataNum, inStateNum, initState=None):
        # 0:current value
        # 1:previous value
        self.inDataNum = inDataNum
        self.inStateNum = inStateNum
        self.inData = np.zeros((inDataNum, 2))
        self.inState = np.zeros((inStateNum, 2))

        if initState is None:
            self.stateMap = [[0]*inStateNum]
        else:
            self.stateMap = [initState]

        self.stateDict = {}
        self.mainSta = []

        # list -> nparray
        self.stateTrans = []
        self.inits = []
        self.finals = []
        self.events = []

    def initState(self, initState):
        self.inState[:, 1] = np.asarray(initState)
        self.inState[:, 0] = np.asarray(initState)
        # self.inState[:,0] = np.zeros(self.inStateNum)

    def endproc(self):
        self.inData[:, 1] = self.inData[:, 0]
        self.inState[:, 1] = self.inState[:, 0]

    def setInput(self, inData):
        self.inData = inData

    def getState(self):
        print "state is"
        print self.inState

    def saveState(self, name, state):
        self.stateDict.update({tuple(state): name})
        self.mainSta.append(state)

    def getInput(self):
        print "input is"
        print self.inData

    def getEvents(self):
        print "{0} events detected".format(len(self.events))
        print "initState, finalState, event"
        for init, final, event in zip(self.inits, self.finals, self.events):
            print init, final, event

    def searchRoot(self, start):
        for stateTran in self.stateTrans:
            if (stateTran[0] == start).all():
                yield stateTran[1].tolist()

    def saiki(self, start, goal):
        nextInits = []
        for final in self.searchRoot(start):
            if final == goal:
                return True
            else:
                nextInits.append(final)

        if nextInits == []:
            return False

        noRoot = True
        for nextInit in nextInits:
            noRoot = noRoot and (nextInit in self.mainSta)

        if noRoot is True:
            return False

        for init in nextInits:
            return self.saiki(init, goal)

    def getMainStaTra(self, mask=None):
        stateTrans = []
        # print self.saiki([0,1,0,0], [0,0,0,1])

        for start, goal in list(itertools.permutations(self.mainSta, 2)):
            if self.saiki(start, goal):
                print start, '->', goal
                stateTrans.append([start, goal])
        return stateTrans

    def getStateTrans(self, mask=None):
        if mask is None:
            mask = self.inStateNum
        for init, final in zip(self.inits, self.finals):
            init = init[:mask].tolist()
            final = final[:mask].tolist()

            if (init + final not in self.stateTrans) and init != final:
                self.stateTrans.append(init + final)

        self.stateTrans = np.array(self.stateTrans)
        self.stateTrans = self.stateTrans.reshape(len(self.stateTrans), 2, mask)
        print '==================='
        print "{0} trans detected ".format(len(self.stateTrans))
        print '==================='

        stateTrans = []
        for stateTran in self.stateTrans:
            init = self.stateDict.get(tuple(stateTran[0]))
            final = self.stateDict.get(tuple(stateTran[1]))
            if init is None:
                init = stateTran[0]
            if final is None:
                final = stateTran[1]

            print init, "->", final
            stateTrans.append([init, final])

        return stateTrans
            # print tuple(stateTran[0]) ,"->",tuple(stateTran[1])


    def getStateMap(self):
        print '==================='
        print "{0} states detected".format(len(self.stateMap))
        print '==================='
        for trans in self.stateMap:
            print trans
        return self.stateMap

    def genAllPattern(self):
        print "this is genAllPattern"
        patterns = np.zeros((4**self.inDataNum, self.inDataNum, 2))
        for i in range(4**self.inDataNum):
            for xIndex in range(self.inDataNum):
                elms = (i >> xIndex*2) & 0b11
                for yIndex in range(2):
                    patterns[i, xIndex, yIndex] = (elms >> yIndex) & 0b1
        return patterns

    def genSglPattern(self):
        print "this is genSglPattern"
        patterns = np.zeros((4*self.inDataNum, self.inDataNum, 2))
        patternNum = 0
        for xIndex in range(self.inDataNum):
            for elm in range(4):
                for yIndex in range(2):
                    patterns[patternNum, xIndex, yIndex] = (elm >> yIndex) & 0b1

                patternNum += 1
        return patterns

    def genPattern(self, mode=None):
        print "generating patterns"
        if mode == "single":
            return self.genSglPattern()
        else:
            return self.genAllPattern()

    def getCode(self, func):
        self.scan = func

    def searchEvent(self, mode=None):
        patterns = self.genPattern(mode)
        print "seaching..."
        for initState in self.stateMap:
            for pattern in patterns:
                self.initState(initState)
                self.setInput(pattern)
                self.scan(self)

                if (self.inState[:, 0] != self.inState[:, 1]).any():
                    init = self.inState[:, 1].tolist()
                    final = self.inState[:, 0].tolist()
                    event = self.inData[:, 0].tolist()
                    self.inits.append(init)
                    self.finals.append(final)
                    self.events.append(event)
                    if final not in self.stateMap:
                        self.stateMap.append(final)
        self.inits = np.asarray(self.inits)
        self.finals = np.asarray(self.finals)
        self.events = np.asarray(self.events)

        self.stateMap = np.asarray(self.stateMap)
        print "{0} pattern tested".format(len(self.stateMap)*len(patterns))

if __name__ == "__main__":
    plc = PLC(1, 2)

    inputs = range(2)
    outputs = range(2,5)
    print inputs
    print outputs



