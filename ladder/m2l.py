class Mnemo(object):
    def __init__(self):
        self.stkMem = []
        self.opeMem = []

    @staticmethod
    def __rise(arg):
        if (arg[0] == 1) and (arg[1] == 0):
            return True
        else:
            return False

    def END(self):
        self.__init__()

    def OUT(self, arg):
        arg[0] = self.opeMem[-1]

    def LD(self, arg):
        arg = bool(arg[0])
        self.opeMem.append(arg)

    def LDI(self, arg):
        arg = not bool(arg[0])
        self.opeMem.append(arg)

    def LDP(self, arg):
        arg = self.__rise(arg)
        self.opeMem.append(arg)

    def ANI(self, arg):
        arg = bool(arg[0])
        self.opeMem[-1] &= (not arg)

    def AND(self, arg):
        arg = bool(arg[0])
        self.opeMem[-1] &= arg

    def OR(self, arg):
        arg = bool(arg[0])
        self.opeMem[-1] |= arg

    def MPS(self):
        self.stkMem.append(self.opeMem[-1])

    def MRD(self):
        self.opeMem[-1] = self.stkMem[-1]

    def MPP(self):
        self.opeMem[-1] = self.stkMem[-1]
        self.stkMem.pop(-1)

    def ORB(self):
        self.opeMem[-2] = self.opeMem[-1] or self.opeMem[-2]
        self.opeMem.pop(-1)

    def ANB(self):
        self.opeMem[-2] = self.opeMem[-1] or self.opeMem[-2]
        self.opeMem.pop(-1)

    def SET(self, arg):
        if self.opeMem[-1] == True:
            arg[0] = True
        
    def RST(self, arg):
        if self.opeMem[-1] == True:
            arg[0] = False
        

    def getMem(self):
        print "----------------"
        print "curMem is"
        print self.opeMem
        print "stkMem is"
        print self.stkMem

if __name__ == "__main__":

    hold = Mnemo()

