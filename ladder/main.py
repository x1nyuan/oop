# coding: utf-8
import ladder as ls
from statemachine import StateMachine
from m2l import Mnemo


def settest(self):
    mn = Mnemo()

    # 0001 -> 
    mn.LDI(self.inState[0])
    mn.ANI(self.inState[1])
    mn.ANI(self.inState[2])
    mn.AND(self.inState[3])
    mn.AND(self.inData[0])
    mn.SET(self.inState[0])
    mn.RST(self.inState[3])

    mn.LD(self.inState[0])
    mn.ANI(self.inState[1])
    mn.ANI(self.inState[2])
    mn.ANI(self.inState[3])
    mn.AND(self.inData[1])
    mn.SET(self.inState[1])
    mn.RST(self.inState[0])
    
    mn.LDI(self.inState[0])
    mn.AND(self.inState[1])
    mn.ANI(self.inState[2])
    mn.ANI(self.inState[3])
    mn.AND(self.inData[3])
    mn.SET(self.inState[0])
    mn.RST(self.inState[1])

    mn.LDI(self.inState[0])
    mn.AND(self.inState[1])
    mn.ANI(self.inState[2])
    mn.ANI(self.inState[3])
    mn.AND(self.inData[4])
    mn.SET(self.inState[3])
    mn.RST(self.inState[1])

    mn.LDI(self.inState[0])
    mn.AND(self.inState[1])
    mn.ANI(self.inState[2])
    mn.ANI(self.inState[3])
    mn.AND(self.inData[5])
    mn.SET(self.inState[2])
    mn.RST(self.inState[1])

    mn.LDI(self.inState[0])
    mn.ANI(self.inState[1])
    mn.AND(self.inState[2])
    mn.ANI(self.inState[3])
    mn.AND(self.inData[6])
    mn.SET(self.inState[3])
    mn.RST(self.inState[2])

if __name__ == "__main__":

    scon = ls.LadderSim(inDataNum=7, inStateNum=4, initState=[0, 0, 0, 1])
    #
    # scon.saveState("Org", [1, 0, 0, 0])
    # scon.saveState("Run", [0, 1, 0, 0])
    # scon.saveState("Err", [0, 0, 1, 0])
    # scon.saveState("Stp", [0, 0, 0, 1])

    scon.saveState([1, 0, 0, 0], [1, 0, 0, 0])
    scon.saveState([0, 1, 0, 0], [0, 1, 0, 0])
    scon.saveState([0, 0, 1, 0], [0, 0, 1, 0])
    scon.saveState([0, 0, 0, 1], [0, 0, 0, 1])

    scon.getCode(settest)
    scon.searchEvent(mode="single")
    # scon.searchEvent()
    scon.getStateMap()
    scon.getStateTrans()
    trans = scon.getMainStaTra()
    # scon.getEvents()

    statemachine = StateMachine('Scon')

    for tran in trans:
        init = tran[0]
        final = tran[1]

        statemachine.edge(u'{0}'.format(init), u'{0}'.format(final), u'')


    statemachine.csv_saveTrans("aaa")
    statemachine.view()
