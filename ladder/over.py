import inspect

def overload(funcOrType, map={}, type=None):
    if not inspect.isclass(funcOrType):
        # We have a function so we are dealing with "@overload"
        if(type):
            map[type] = funcOrType
        else:
            map['default_function'] = funcOrType
    else:
        def overloadWithType(func):
            return overload(func, map, funcOrType)
        return  overloadWithType

    def doOverload(*args, **kwargs):
        for type in [t for t in map.keys() if t != 'default_function'] :
            if isinstance(args[1], type): # Note args[0] is 'self' i.e. MyClass instance.
                return map[type](*args, **kwargs)

        return map['default_function'](*args, **kwargs)

    return doOverload

class MyClass(object):
    def __init__(self):
        self.some_instance_var = 1

    def print_first_item(self, a, b):
        print a
        print b

    # @overload
    # def print_first_item(self, c, d):
    #     print c
    #     print d


if __name__ == '__main__':
    m = MyClass()
    print m.print_first_item.__name__ 

