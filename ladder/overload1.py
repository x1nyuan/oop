from collections import defaultdict

def determine_types(kwargs):
    return tuple([(k, type(v)) for k,v in kwargs.iteritems()])
           

function_table = defaultdict(dict)
def overload(kwarg_types=()):
    def wrap(func):
        named_func = function_table[func.__name__]
        named_func[kwarg_types] = func
        print function_table
        def call_function_by_signature(*args, **kwargs):
            return named_func[determine_types(kwargs)](*args,**kwargs)
        return call_function_by_signature
    return wrap

class MyClass(object):
    def __init__(self):
        self.some_instance_var = 1

    @overload((('a',int),('b',int)))
    def print_first_item(self, b, a):
        print a
        print b

    @overload((('c',int),))
    def print_first_item(self, c):
        print c 

if __name__ == '__main__':
    m = MyClass()
    m.print_first_item(b=1, a=2)
    m.print_first_item(c=3)

    def aaa(*args, **kwargs):
        print kwargs
    aaa(bint=1,ind=1, bit1=2, index0=1, bit=1, index1=1, num=0, a0= 3)

