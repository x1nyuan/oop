# coding: utf-8
import numpy as np
import pandas as pd
import time
from overload import overload

# utlity function
def get_bit(wrdDev, bit):
    return bool(int(format(wrdDev, 'b').zfill(16)[-bit-1]))

def get_int(bitDevs, start, bit):
    num = 0
    for i, elem in enumerate(bitDevs[start:start+bit]):
        num += elem*2**i
    return num

def EQ(num0, num1):
    if num0 == num1:
        return True
    else:
        return False

class Device(object):
    def __init__(self,nBit, nWrd, nBuf, nTmr):
        # nBit, nWrd, nBuf = devSize
        self.bitDevs = np.zeros((nBit, ), dtype=bool)
        self.wrdDevs = np.zeros((nWrd, ), dtype=np.int16)
        self.bitBufs = np.zeros((nBuf, ), dtype=bool)
        self.tmrDevs = np.zeros((nTmr, ))
        self.trgDevs = np.zeros((nTmr, ))
        self.stkMem = []
        self.opeMem = []
        self.bufIndex = 0

    def _spedev(self, sindex):
        if sindex == 400 or sindex == 1036:
             return True
        elif sindex == 401 or sindex == 1037:
            return False
        else:
            print 'unknown index'

    def _timer(self, timer):
        return (time.time() - self.tmrDevs[timer]) >= 0.1*self.trgDevs[timer]

    @overload((),(('wrdAdr0', int), ('wrdAdr1', int)))
    def __oprand(self, wrdAdr0, wrdAdr1):
        return self.wrdDevs[wrdAdr0], self.wrdDevs[wrdAdr1]

    # ==================OUT============================
    # normal bit dev
    @overload((), (('index0', int),))
    def OUT(self, index0):
        self.bitDevs[index0] = self.opeMem[-1]
    # wrd bitfield
    @overload((), (('index0', int), ('bit', int)))
    def OUT(self, index0, bit):
        if self.opeMem[-1] == True :
            self.wrdDevs[index0] |= (1<<bit)
        else:
            self.wrdDevs[index0] &= ~(1<<bit)
    # timer
    @overload((), (('num1', int), ('timer0', int))) # dic の格納順のため
    def OUT(self, timer0, num1):
        if self.opeMem[-1] == True :
            if self.tmrDevs[timer0] == 0.:
                self.tmrDevs[timer0] = time.time()
                self.trgDevs[timer0] = num1
        else:
            self.tmrDevs[timer0] = 0.

    # ==================OUTH============================
    def OUTH(self, timer0, num1):
        if self.opeMem[-1] == True:
            if self.tmrDevs[timer0] == 0.:
                self.tmrDevs[timer0] = time.time()
                self.trgDevs[timer0] = 0.01*num1
        else:
            self.tmrDevs[timer0] == 0.

    # ==================LD============================
    # normal bit device
    @overload((),(('index0', int),))
    def LD(self, index0):
        result = self.bitDevs[index0]
        self.opeMem.append(result)
    # special bit device
    @overload((),(('sindex0', int),))
    def LD(self, sindex0):
        result = self._spedev(sindex0)
        self.opeMem.append(result)
    # wrd bit field
    @overload((),(('index0', int), ('bit', int)))
    def LD(self, index0, bit):
        result = get_bit(self.wrdDevs[index0], bit)
        self.opeMem.append(result)
    @overload((),(('timer0', int),))
    def LD(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            result = False
        else:
            result = self._timer(timer0)
        self.opeMem.append(result)

    # ==================LDI============================
    # [XYML]d+
    @overload((),(('index0', int),))
    def LDI(self, index0):
        result = self.bitDevs[index0]
        self.opeMem.append(not result)
    # SM\d+
    @overload((),(('sindex0', int),))
    def LDI(self, sindex0):
        result = self._spedev(sindex0)
        self.opeMem.append(not result)
    # D\d+\.\d
    @overload((),(('index0', int), ('bit', int)))
    def LDI(self, index0, bit):
        result = get_bit(self.wrdDevs[index0], bit)
        self.opeMem.append(not result)
    # T\d+
    @overload((),(('timer0', int),))
    def LDI(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            result = False
        else:
            result = self._timer(timer0)
        self.opeMem.append(not result)

    # ==================LDP============================
    # [XYML]d+
    def LDP(self, index0):
        current = self.bitDevs[index0]
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # SM\d+
    @overload((),(('sindex0', int),))
    def LDP(self, sindex0):
        current = self._spedev(sindex0)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # D\d+\.\d
    @overload((),(('index0', int), ('bit', int)))
    def LDP(self, index0, bit):
        current = get_bit(self.wrdDevs[index0], bit)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # T\d+
    @overload((),(('timer0', int),))
    def LDP(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            current = False
        else:
            current = self._timer(timer0)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1

    # ==================LDF============================
    # [XYML]d+
    @overload((),(('index0', int),))
    def LDF(self, index0):
        current = self.bitDevs[index0]
        if self.bitBufs[self.bufIndex] == True and current == False:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # SM\d+
    @overload((),(('sindex0', int),))
    def LDF(self, sindex0):
        current = self._spedev(sindex0)
        if self.bitBufs[self.bufIndex] == True and current == False:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # D\d+\.\d
    @overload((),(('index0', int), ('bit', int)))
    def LDF(self, index0, bit):
        current = get_bit(self.wrdDevs[index0], bit)
        if self.bitBufs[self.bufIndex] == True and current == False:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # T\d+
    @overload((),(('timer0', int),))
    def LDF(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            current = False
        else:
            current = self._timer(timer0)
        if self.bitBufs[self.bufIndex] == True and current == False:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1

    # ==================LDE============================
    # 9 overloads
    # \d+ == \d+
    @overload((),(('num0', int), ('num1', int)))
    def LDE(self, num0, num1):
        if num0 == num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == D\d+
    @overload((),(('index0', int), ('index1', int)))
    def LDE(self, index0, index1):
        if self.wrdDevs[index0] == self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def LDE(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) == get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDE(self, num0, index1):
        if num0 == self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDE(self, num1, index0):
        if num1 == self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def LDE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) == num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) == num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('index0', int) ))
    def LDE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) == self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) == self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================LDN============================
    # 9 overloads
    # \d+ == \d+
    @overload((),(('num0', int), ('num1', int)))
    def LDN(self, num0, num1):
        if num0 != num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == D\d+
    @overload((),(('index0', int), ('index1', int)))
    def LDN(self, index0, index1):
        if self.wrdDevs[index0] != self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def LDN(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) != get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ != D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDN(self, num0, index1):
        if num0 != self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ != \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDN(self, num1, index0):
        if num1 != self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ != K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def LDN(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) != num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ != \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDN(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) != num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('index0', int) ))
    def LDN(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) != self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDN(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) != self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================LDL============================
    # 9 overloads
    # \d+ == \d+
    @overload((),(('num0', int), ('num1', int)))
    def LDL(self, num0, num1):
        if num0 < num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == D\d+
    @overload((),(('index0', int), ('index1', int)))
    def LDL(self, index0, index1):
        if self.wrdDevs[index0] < self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def LDL(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) < get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDL(self, num0, index1):
        if num0 < self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDL(self, num1, index0):
        if num1 < self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def LDL(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) < num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDL(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) < num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('index0', int) ))
    def LDL(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) < self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDL(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) < self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================LDLE============================
    # 9 overloads
    # \d+ == \d+
    @overload((),(('num0', int), ('num1', int)))
    def LDLE(self, num0, num1):
        if num0 <= num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == D\d+
    @overload((),(('index0', int), ('index1', int)))
    def LDLE(self, index0, index1):
        if self.wrdDevs[index0] <= self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def LDLE(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) <= get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDLE(self, num0, index1):
        if num0 <= self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDLE(self, num1, index0):
        if num1 <= self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def LDLE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) <= num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDLE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) <= num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('index0', int) ))
    def LDLE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) <= self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDLE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) <= self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================LDG============================
    # 9 overloads
    # \d+ == \d+
    @overload((),(('num0', int), ('num1', int)))
    def LDG(self, num0, num1):
        if num0 > num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == D\d+
    @overload((),(('index0', int), ('index1', int)))
    def LDG(self, index0, index1):
        if self.wrdDevs[index0] > self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def LDG(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) > get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDG(self, num0, index1):
        if num0 > self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDG(self, num1, index0):
        if num1 > self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == K\d+[XYML]\d+
    @overload((), (('itr1', int), ('index1', int), ('num0', int) ))
    def LDG(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) > num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDG(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) > num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('index0', int) ))
    def LDG(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) > self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((), (('itr0', int), ('index0', int), ('num1', int) ))
    def LDG(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) > self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================LDGE============================
    # 9 overloads
    # \d+ == \d+
    @overload((),(('num0', int), ('num1', int)))
    def LDGE(self, num0, num1):
        if num0 >= num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == D\d+
    @overload((),(('index0', int), ('index1', int)))
    def LDGE(self, index0, index1):
        if self.wrdDevs[index0] >= self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def LDGE(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) >= get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDGE(self, num0, index1):
        if num0 >= self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDGE(self, num1, index0):
        if num1 >= self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == K\d+[XYML]\d+
    @overload((), (('itr1', int), ('index1', int), ('num0', int) ))
    def LDGE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) >= num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDGE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) >= num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('index0', int) ))
    def LDGE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) >= self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((), (('itr0', int), ('index0', int), ('num1', int) ))
    def LDGE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) >= self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================AND============================
    # normal bit device
    @overload((), (('index0', int), ))
    def AND(self, index0):
        result = self.bitDevs[index0]
        self.opeMem[-1] &= result
    # wrd bit field
    @overload((), (('index0', int), ('bit', int)))
    def AND(self, index0, bit):
        result = get_bit(self.wrdDevs[index0], bit)
        self.opeMem[-1] &= result
    # timer
    @overload((), (('timer0', int), ))
    def AND(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            result = False
        else:
            result = self._timer(timer0)
        self.opeMem[-1] &= result

    # ==================ANI============================
    # normal bit device
    @overload((), (('index0', int),))
    def ANI(self, index0):
        result = self.bitDevs[index0]
        self.opeMem[-1] &= (not result)
    # wrd bit field
    @overload((), (('index0', int), ('bit', int)))
    def ANI(self, index0, bit):
        result = get_bit(self.wrdDevs[index0], bit)
        self.opeMem[-1] &= (not result)
    # timer
    @overload((), (('timer0', int), ))
    def ANI(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            result = False
        else:
            result = self._timer(timer0)
        self.opeMem[-1] &= (not result)

    # ==================OR============================
    # normal bit device
    @overload((), (('index0', int), ))
    def OR(self, index0):
        result = self.bitDevs[index0]
        self.opeMem[-1] |= result
    # wrd bit field
    @overload((), (('index0', int), ('bit', int)))
    def OR(self, index0, bit):
        result = get_bit(self.wrdDevs[index0], bit)
        self.opeMem[-1] |= result
    # timer
    @overload((), (('timer0', int), ))
    def OR(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            result = False
        else:
            result = self._timer(timer0)
        self.opeMem[-1] |= result

    # ==================ORI============================
    # normal bit device
    @overload((), (('index0', int), ))
    def ORI(self, index0):
        result = self.bitDevs[index0]
        self.opeMem[-1] |= (not result)
    # wrd bit field
    @overload((), (('bit', int), ('index0', int)))
    def ORI(self, index0, bit):
        result = get_bit(self.wrdDevs[index0], bit)
        self.opeMem[-1] |= (not result)
    # timer
    @overload((), (('timer0', int), ))
    def ORI(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            result = False
        else:
            result = self._timer(timer0)
        self.opeMem[-1] |= (not result)

    # ==================ORN============================
    # 5 overloads
    # \d+ != D\d+
    @overload((),(('index1', int), ('num0', int)))
    def ORN(self, num0, index1):
        if num0 != self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem[-1] = result
    # D\d+ != \d+
    @overload((),(('index0', int), ('num1', int)))
    def ORN(self, num1, index0):
        if num1 != self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # \d+ != K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def ORN(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) != num0:
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # K\d+[XYML]\d+ != \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def ORN(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) != num1:
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # K\d+[XYML]\d+ != K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def ORN(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) != get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem[-1] |= result


    # ==================ANDP============================
    # [XYML]d+
    @overload((),(('index0', int),))
    def ANDP(self, index0):
        current = self.bitDevs[index0]
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # SM\d+
    @overload((),(('sindex0', int),))
    def ANDP(self, sindex0):
        current = self._spedev(sindex0)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # D\d+\.\d
    @overload((),(('index0', int), ('bit', int)))
    def ANDP(self, index0, bit):
        current = get_bit(self.wrdDevs[index0], bit)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # T\d+
    @overload((),(('timer0', int),))
    def ANDP(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            current = False
        else:
            current = self._timer(timer0)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1

    # ==================ANDF============================
    # [XYML]d+
    @overload((),(('index0', int),))
    def ANDF(self, index0):
        current = self.bitDevs[index0]
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem.append(result)
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # SM\d+
    @overload((),(('sindex0', int),))
    def ANDF(self, sindex0):
        current = self._spedev(sindex0)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # D\d+\.\d
    @overload((),(('index0', int), ('bit', int)))
    def ANDF(self, index0, bit):
        current = get_bit(self.wrdDevs[index0], bit)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1
    # T\d+
    @overload((),(('timer0', int),))
    def ANDF(self, timer0):
        if self.tmrDevs[timer0] == 0.:
            current = False
        else:
            current = self._timer(timer0)
        if self.bitBufs[self.bufIndex] == False and current == True:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
        self.bitBufs[self.bufIndex] = current
        self.bufIndex += 1

    # ==================BCD============================
    # BIN to BCD conversion
    # D\d+ -> D\d+
    @overload((), (('index0', int), ('index1', int)))
    def BCD(self, index0, index1):
        if self.opeMem[-1] == True:
            num = int(format(self.wrdDevs[index0]), 16)
            self.wrdDevs[index1] = num
    # [HK]\d+ -> D\d+
    @overload((), (('index1', int), ('num0', int)))
    def BCD(self, num0, index1):
        if self.opeMem[-1] == True:
            num = int(format(num0), 16)
            self.wrdDevs[index1] = num
    # [K]\d+[XYML]\d+ -> D\d+
    @overload((), (('itr0', int), ('index0', int), ('index1', int)))
    def BCD(self, itr0, index0, index1):
        if self.opeMem[-1] == True:
            num = int(format(get_int(self.bitDevs, index0, itr0*4)), 16)
            self.wrdDevs[index1] = num
    # D\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('index0', int), ('index1', int)))
    def BCD(self, itr1, index0, index1):
        if self.opeMem[-1] == True:
            num = int(format(self.wrdDevs[index0]), 16)
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])
    # [HK]\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('index0', int), ('num0', int)))
    def BCD(self, num0, itr1, index1):
        if self.opeMem[-1] == True:
            num = int(format(num0), 16)
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])
    # [K]\d+[XYML]\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int)))
    def BCD(self, itr0, index0, itr1, index1):
        if self.opeMem[-1] == True:
            num = int(format(get_int(self.bitDevs, index0, itr0*4)), 16)
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])

    # ==================BIN============================
    # BCD to BIN conversion
    # D\d+ -> D\d+
    @overload((), (('index0', int), ('index1', int)))
    def BIN(self, index0, index1):
        if self.opeMem[-1] == True:
            num = int(format(self.wrdDevs[index0], 'x'))
            self.wrdDevs[index1] = num
    # [HK]\d+ -> D\d+
    @overload((), (('index1', int), ('num0', int)))
    def BIN(self, num0, index1):
        if self.opeMem[-1] == True:
            num = int(format(num0, 'x'))
            self.wrdDevs[index1] = num
    # [K]\d+[XYML]\d+ -> D\d+
    @overload((), (('itr0', int), ('index0', int), ('index1', int)))
    def BIN(self, itr0, index0, index1):
        if self.opeMem[-1] == True:
            num = int(format(get_int(self.bitDevs, index0, itr0*4), 'x'))
            self.wrdDevs[index1] = num
    # D\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('index0', int), ('index1', int)))
    def BIN(self, itr1, index0, index1):
        if self.opeMem[-1] == True:
            num = int(format(self.wrdDevs[index0], 'x'))
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])
    # [HK]\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('index0', int), ('num0', int)))
    def BIN(self, num0, itr1, index1):
        if self.opeMem[-1] == True:
            num = int(format(num0, 'x'))
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])
    # [K]\d+[XYML]\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int)))
    def BIN(self, itr0, index0, itr1, index1):
        if self.opeMem[-1] == True:
            num = int(format(get_int(self.bitDevs, index0, itr0*4), 'x'))
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])

    # ==================MOV============================ 
    # 6 overload
    # D\d+ -> D\d+
    @overload((),(('index0',int), ('index1', int)))
    def MOV(self, index0, index1):
        if self.opeMem[-1] == True:
            self.wrdDevs[index1] = self.wrdDevs[index0]
    # [HK]\d+ -> D\d+
    @overload((),(('index1', int), ('num0', int)))
    def MOV(self, num0, index1):
        if self.opeMem[-1] == True:
            num = num0
            self.wrdDevs[index1] = num
    # [K]\d+[XYML]\d+ -> D\d+
    @overload((),(('itr0', int), ('index0', int), ('index1', int)))
    def MOV(self, itr0, index0, index1):
        if self.opeMem[-1] == True:
            num = get_int(self.bitDevs, index0, itr0*4)
            self.wrdDevs[index1] = num
    # D\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('index0', int), ('index1', int)))
    def MOV(self, itr1, index0, index1):
        if self.opeMem[-1] == True:
            num = self.wrdDevs[index0]
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])
    # [HK]\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('index0', int), ('num0', int)))
    def MOV(self, num0, itr1, index1):
        if self.opeMem[-1] == True:
            binstr = format(num0, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])
    # [K]\d+[XYML]\d+ -> [K]\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int)))
    def MOV(self, itr0, index0, itr1, index1):
        if self.opeMem[-1] == True:
            num = get_int(self.bitDevs, index0, itr0*4)
            binstr = format(num, 'b').zfill(16)
            for i in range(itr1*4):
                self.bitDevs[index1+i] = int(binstr[-i-1])

    # ==================MOV============================ 
    # D\d+ -> D\d+
    def MOVP(self, index1, index0=None, num0=None):
        if index0 is not None:
            num = self.wrdDevs[index0]
        else:
            num = num0

        if self.bitBufs[self.bufIndex] == False and self.opeMem[-1] == True:
            self.wrdDevs[index1] = num

    # ==================ORE============================ 
    # 5 overloads
    # \d+ <= D\d+
    @overload((),(('index1', int), ('num0', int)))
    def ORE(self, num0, index1):
        if num0 == self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # D\d+ <= \d+
    @overload((),(('index0', int), ('num1', int)))
    def ORE(self, num1, index0):
        if  self.wrdDevs[index0] == num1 :
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # \d+ <= K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def ORE(self, num0, index1, itr1):
        if num0 == get_int(self.bitDevs, index1, itr1*4):
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # K\d+[XYML]\d+ <= \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def ORE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) == num1:
            result = True
        else:
            result = False
        self.opeMem[-1] |= result
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr0', int), ('itr1', int), ('index0', int), ('index1', int) ))
    def ORE(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) == get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem[-1] |= result

    def DMOV(self, index1, index0=None, num0=None):
        if index0 is not None:
            num = self.wrdDevs[index0]
        else:
            num = num0
        if self.opeMem[-1] == True:
            result = num & 0xffff
            self.wrdDevs[index1] = result
            result = (num >> 16) & 0xffff
            self.wrdDevs[index1+1] = result

    def WAND(self, index0=None, num0=None, num1=None, index2=None, index1=None):
        if self.opeMem[-1] == True:
            if index0 is not None and index2 is not None:
                self.wrdDevs[index2] = self.wrdDevs[index0] and num1
            elif num0 is not None and index1 is not None:
                self.wrdDevs[index1] = self.wrdDevs[index1] and num0

    def BMOV(self, index0, index1, num2):
        if self.opeMem[-1] == True:
            self.wrdDevs[index1:index1+num2] = self.wrdDevs[index0:index0+num2]

    def FMOV(self, index1, num2, index0=None, num0=None):
        if index0 is not None:
            num = self.wrdDevs[index0]
        if num0 is not None:
            num = num0

        if self.opeMem[-1] == True:
            self.wrdDevs[index1:index1+num2] = num



    # ==================LDE============================
    # 5 overloads
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def LDE(self, num0, index1):
        if num0 == self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def LDE(self, num1, index0):
        if num1 == self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # \d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def LDE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) == num0:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def LDE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) == num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr0', int), ('itr1', int), ('index0', int), ('index1', int) ))
    def LDE(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) == get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem.append(result)

    # ==================ANDE============================
    # 5 overloads
    # \d+ == D\d+
    @overload((),(('index1', int), ('num0', int)))
    def ANDE(self, num0, index1):
        if num0 == self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # D\d+ == \d+
    @overload((),(('index0', int), ('num1', int)))
    def ANDE(self, num1, index0):
        if num1 == self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # \d+ == K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def ANDE(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) == num0:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # K\d+[XYML]\d+ == \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def ANDE(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) == num1:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # K\d+[XYML]\d+ == K\d+[XYML]\d+
    @overload((),(('itr0', int), ('itr1', int), ('index0', int), ('index1', int) ))
    def ANDE(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) == get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem[-1] &= result


    def ANDGE(self, num0=None,index0=None, index1=None, num1=None):
        if index0 is not None and num1 is not None:
            if self.wrdDevs[index0] <= num1:
                result = True
            else:
                result = False

        elif index1 is not None and num0 is not None:
            if  num0 <= self.wrdDevs[index1]:
                result = True
            else:
                result = False

        self.opeMem[-1] &= result

    def ANDL(self,  index1=None, index0=None, num1=None, num0=None):
        if index0 is None:
            if self.wrdDevs[index1] < num0:
                result = True
            else:
                result = False
        else:
            if self.wrdDevs[index0] < num1:
                result = True
            else:
                result = False

        self.opeMem[-1] &= result

    # ==================ANDN============================
    # 5 overloads
    # \d+ != D\d+
    @overload((),(('index1', int), ('num0', int)))
    def ANDN(self, num0, index1):
        if num0 != self.wrdDevs[index1]:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # D\d+ != \d+
    @overload((),(('index0', int), ('num1', int)))
    def ANDN(self, num1, index0):
        if num1 != self.wrdDevs[index0]:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # \d+ != K\d+[XYML]\d+
    @overload((),(('itr1', int), ('index1', int), ('num0', int) ))
    def ANDN(self, num0, index1, itr1):
        if get_int(self.bitDevs, index1, itr1*4) != num0:
            result = True
        else:
            result = False
        self.opeMem[-1] &= result
    # K\d+[XYML]\d+ != \d+
    @overload((),(('itr0', int), ('index0', int), ('num1', int) ))
    def ANDN(self, num1, index0, itr0):
        if get_int(self.bitDevs, index0, itr0*4) != num1:
            result = True
        else:
            result = False
        self.opeMem.append(result)
    # K\d+[XYML]\d+ != K\d+[XYML]\d+
    @overload((),(('itr1', int), ('itr0', int), ('index0', int), ('index1', int) ))
    def ANDN(self, itr0, itr1, index0, index1):
        if get_int(self.bitDevs, index1, itr1*4) != get_int(self.bitDevs, index0, itr0*4):
            result = True
        else:
            result = False
        self.opeMem[-1] &= result

    def ORGE(self, index, rIndex=None, num=None):
        if rIndex is not None:
            num = self.wrdDevs[rIndex]

        if self.wrdDevs[index] <= num:
            result = True
        else:
            result = False
        self.opeMem[-1] |= result

    # ==================No args method============================
    def INV(self):
        self.opeMem[-1] = not self.opeMem[-1]

    def MEP(self):
        if self.bitBufs[self.bufIndex] == False and self.opeMem[-1]:
            self.opeMem[-1] = True
        else:
            self.opeMem[-1] = False

        self.bitBufs[self.bufIndex] = self.opeMem[-1]
        self.bufIndex += 1

    def MPS(self):
        self.stkMem.append(self.opeMem[-1])

    def MRD(self):
        self.opeMem[-1] = self.stkMem[-1]

    def MPP(self):
        self.opeMem[-1] = self.stkMem[-1]
        self.stkMem.pop(-1)

    def ORB(self):
        self.opeMem[-2] = self.opeMem[-1] or self.opeMem[-2]
        self.opeMem.pop(-1)

    def ANB(self):
        self.opeMem[-2] = self.opeMem[-1] and self.opeMem[-2]
        self.opeMem.pop(-1)

    def SET(self, index):
        if self.opeMem[-1] is True:
            self.bitDevs[index] = True

    def RST(self, index0):
        if self.opeMem[-1] is True:
            self.bitDevs[index0] = False

    def END(self):
        self.bufIndex = 0

    def NOPLF(self):
        pass

    def getMem(self):
        print "----------------"
        print 'OpeMem',self.opeMem, 'stk', self.stkMem
        print "----------------"

    # eqation


if __name__ == "__main__":
    dev = Device(3,3,3,3)
    dev.getMem()
