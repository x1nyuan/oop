# coding: utf-8
import numpy as np
import time
from dev import Device
from translator import Translator
from overload import overload


class PLC(object):
    def __init__(self, path, nWrd=None):
        ladder = Translator(path)
        # ladder.transrate(debug=True)
        ladder.transrate()
        self.bitLabels, self.wrdLabels, self.tmrLabels = ladder.devLabels
        self.ladder_logic = ladder.get_func()
        nBit, nWrdtemp, nBuf, nTmr= ladder.devSize
        if nWrd is None:
            nWrd = nWrdtemp
        self.dev = Device(nBit=nBit, nWrd=nWrd, nBuf=nBuf, nTmr=nTmr)

    @overload((), (('bitDev', str), ('val', bool)))
    def set_dev(self, bitDev, val=True):
        print bitDev
        index = self.bitLabels.index(bitDev)
        self.dev.bitDevs[index] = val

    @overload((), (('wrdDev', str), ('val', int)))
    def set_dev(self, wrdDev, val=0):
        index = self.wrdLabels.index(wrdDev)
        self.dev.wrdDevs[index] = val


    def get_dev(self, devType, index):
        if devType == 'bit':
            val = self.dev.bitDevs[index]
        elif devType == 'wrd':
            val = self.dev.wrdDevs[index]
        else:
            print('no type')
        return val

    def scan(self):
        self.ladder_logic(self.dev)

    def get_wrdDev(self):
        # print [format(i, 'b').zfill(16) for i in self.dev.wrdDevs]
        print self.dev.wrdDevs

    def get_bitDev(self):
        print self.dev.bitDevs

    def get_bufDev(self):
        print self.dev.bitBufs

if __name__ == "__main__":

    plc = PLC('../test/LDE.csv')

    print plc.bitLabels
    print plc.tmrLabels
    print plc.wrdLabels

    print'in==========='
    plc.get_bitDev()
    plc.get_wrdDev()

    # -f ON
    # plc.set_dev(bitDev='X10E', val=False)
    # plc.set_dev(bitDev='X0', val=True)
    plc.set_dev(bitDev='X0', val=False)
    plc.set_dev(bitDev='X1', val=True)
    plc.set_dev(bitDev='X3', val=True)
    plc.set_dev(bitDev='X5', val=True)
    print'---scan----------------------------------------------'
    plc.scan()

    print'out==========='
    print np.array(plc.bitLabels)[plc.dev.bitDevs]
    print plc.dev.tmrDevs
    print plc.get_wrdDev()





