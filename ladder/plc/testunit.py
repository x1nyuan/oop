# coding: utf-8
from plc import PLC
import numpy as np


class PLCTestUnit(object):
    def __init__(self, inArr, stArr, plc):
        self.inArr = inArr
        self.stArr = stArr
        self.plc = plc
        self.patterns = None
        self.states = []
        self.trans = []

    def gen_pattern(self):
        print "this is input pattern generater"
        inNum = len(self.inArr)
        patterns = np.zeros((2**inNum, inNum), dtype=bool)
        for i in range(2**inNum):
            for index in range(inNum):
                patterns[i, index] = (i >> index) & 0b1
        self.patterns = patterns
        return patterns

    def run_scan(self, event, init):
        # init plc bufs
        state, bitBufs = init
        state = np.array(state)
        bitBufs = np.array(bitBufs)
        self.plc.dev.bitBufs = bitBufs

        # set state to plc.dev
        for i, devIdx in enumerate(self.stArr):
            self.plc.set_dev('bit', devIdx, state[i])


        # set input to plc.dev
        for i, devIdx in enumerate(self.inArr):
            self.plc.set_dev('bit', devIdx, event[i])

        print 'in---------------'
        print self.plc.dev.bitBufs
        print self.plc.dev.bitDevs
        print '---------------'
        self.plc.scan()

        state = np.zeros(len(self.stArr), dtype=bool)
        for i, devIdx in enumerate(self.stArr):
            state[i] = self.plc.get_dev('bit', devIdx)
        print 'out---------------'
        print self.plc.dev.bitDevs
        print '---------------'

        return [state.tolist(), self.plc.dev.bitBufs.tolist()]

    def search(self):
        if self.patterns is None:
            self.gen_pattern()
        state = np.zeros((len(self.stArr)), dtype=bool).tolist()
        init = [state, self.plc.dev.bitBufs.tolist()]
        self.states.append(init)

        print('searching event...')
        for initState in self.states:
            init = initState[0]
            print '========init=========='
            for pattern in self.patterns:
                final, nextBuf = self.run_scan(pattern, initState)
                if [final, nextBuf] not in self.states:
                    print 'append', state
                    self.states.append([final, nextBuf])
                if init != final:
                    self.trans.append([init, final])

        print len(self.trans)
        for tran in self.trans:
            print tran[0],'->', tran[1]




if __name__ == "__main__":

    plc = PLC('../test/LD.csv')
    print plc.bitLabels


    inArr = range(3,5)
    stArr = range(0,3)

    print inArr, stArr

    testUnit = PLCTestUnit(inArr, stArr, plc)
    testUnit.search()
