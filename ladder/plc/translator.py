# coding: utf-8
import pandas as pd
import numpy as np
from collections import defaultdict
import re

class Translator(object):
    def __init__(self, path):
        csv = pd.read_csv(path, header=2).as_matrix()
        self.opcode = csv[:, 2]
        self.oprand = csv[:, 3]

        self.pyCode = []

        self.bitLabels = []
        self.tmrLabels = []
        self.wrdLabels = []

        self.pulseOps = []

        self.collect_dev()
        self.collect_opc()

        self.nBit = len(self.bitLabels)
        self.nWrd = len(self.wrdLabels)
        self.nBuf = len(self.pulseOps)
        self.nTmr = len(self.tmrLabels)
        self.devSize = [self.nBit, self.nWrd, self.nBuf, self.nTmr]
        self.devLabels = [self.bitLabels, self.wrdLabels, self.tmrLabels]

    def arg_convert(self, oprand, index):
        if isinstance(oprand, str):
            m = re.match('[K]\d+[XYML]\d+$', oprand)
            if m is not None:
                itr = re.search('\d+', oprand).group()
                label = re.search('[XYML]\d+', oprand).group()
                return 'itr{0}={1}, index{0}={2}'.format(index, itr, self.bitLabels.index(label))

            m = re.match('[XYML][0-9A-F]+$', oprand)
            if m is not None:
                return 'index{0}={1}'.format(index, self.bitLabels.index(oprand))

            m = re.match('T[0-9A-F]+$', oprand)
            if m is not None:
                return 'timer{0}={1}'.format(index, self.tmrLabels.index(oprand))

            # num
            m = re.match('[K]\d+', oprand)
            if m is not None:
                num = oprand[1:]
                return 'num{0}={1}'.format(index, num)

            m = re.match('[H]\d+', oprand)
            if m is not None:
                num = oprand[1:]
                return 'num{0}=0x{1}'.format(index, num)

            m = re.match('D\d+\.[0-9A-F]', oprand)
            if m is not None:
                splited = re.split('\.', oprand)
                return 'index{0}={1}, bit=0x{2}'.format(index, self.wrdLabels.index(splited[0]), splited[1])

            m = re.match('D\d+$', oprand)
            if m is not None:
                return 'index{0}={1}'.format(index, self.wrdLabels.index(oprand))

            m = re.match('SM\d+$', oprand)
            if m is not None:
                num = oprand[2:]
                return 'sindex{0}={1}'.format(index, num)
        else:
            return ''

    def op_convert(self, opcode):
        if opcode == 'LD<>':
            return 'LDN'
        if opcode == 'LD=':
            return 'LDE'
        elif opcode == 'AND<=':
            return 'ANDGE'
        elif opcode == 'AND>=':
            return 'ANDL'
        elif opcode == 'OR=':
            return 'ORE'
        elif opcode == 'AND>':
            return 'ANDL'
        elif opcode == 'AND<>':
            return 'ANDN'
        elif opcode == 'LD>':
            return 'LDL'
        elif opcode == 'LD<':
            return 'LDG'
        elif opcode == 'AND=':
            return 'ANDE'
        else:
            return opcode

    def transrate(self, path='pymnemo.py', debug=None):
        argCode = np.argwhere(map(lambda x: isinstance(x, str), self.opcode)).reshape(-1)

        dec = 'def ladder(dev):'

        self.pyCode.append(dec)
        for i in range(len(argCode)-1):
            opcode = self.op_convert(self.opcode[argCode[i]])
            line = '    dev.{0}('.format(opcode)
            oprands = filter(lambda x: isinstance(x, str), self.oprand[argCode[i]:argCode[i+1]])

            for i, oprand in enumerate(oprands):
                if i > 0:
                    line += ', '
                line += self.arg_convert(oprand, i)
            line += ')'
            self.pyCode.append(line)
            if debug == True:
                line = '    print "{0}"'.format(line)
                self.pyCode.append(line)
                line = '    dev.getMem()'
                self.pyCode.append(line)
        end = '    dev.END()'
        self.pyCode.append(end)

        f = open(path, 'w')
        for line in self.pyCode:
            f.write(line + '\n')
        f.close()

    def collect_dev(self):
        for oprand in self.oprand:
            if isinstance(oprand, str):
                # search bit dev
                m = re.match('[XYLM][0-9A-F]+$', oprand)
                if m is not None:
                    bitLabel = m.group()
                    if bitLabel not in self.bitLabels:
                        self.bitLabels.append(bitLabel)
                        continue

                m = re.match('T[0-9A-F]+$', oprand)
                if m is not None:
                    tmrLabel = m.group()
                    if tmrLabel not in self.tmrLabels:
                        self.tmrLabels.append(tmrLabel)
                        continue
                # m = re.match('SM[0-9A-F]+', oprand)
                # if m is not None:
                #     bitLabel = m.group()
                #     if bitLabel not in self.bitLabels:
                #         self.bitLabels.append(bitLabel)
                #         continue

                # search wrd dev 
                # before dot
                m = re.match('[D][0-9A-F]+', oprand)
                if m is not None:
                    wrdLabel = m.group()
                    if wrdLabel not in self.wrdLabels:
                        self.wrdLabels.append(wrdLabel)
                        continue

                m = re.match('[K]\d+[XYML]\d+$', oprand)
                if m is not None:
                    itr = int(re.search('\d+', oprand).group())
                    label = re.search('[XYML]', oprand)
                    adr = oprand[label.start()+1:]
                    dtype = label.group()
                    bitLabels = [dtype+format(int(adr, 16)+i, 'x') for i in range(itr*4)]
                    for bitLabel in bitLabels:
                        if bitLabel not in self.bitLabels:
                            self.bitLabels.append(bitLabel)

        self.bitLabels.sort()
        self.wrdLabels.sort()

    def collect_opc(self):
        pulseops = ['LDP', 'LDF', 'MOVP', 'ANDF', 'MEP']
        for opc in self.opcode:
            if isinstance(opc, str):
                if opc in pulseops:
                    self.pulseOps.append(opc)

    def get_func(self):
        from pymnemo import ladder
        return ladder

if __name__ == '__main__':
    scon = Translator('../test/LD.csv')
    scon.transrate()
    scon.get_func()
