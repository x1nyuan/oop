﻿# coding: utf-8
import os
from graphviz import Digraph
import numpy
import itertools
import csv


styles = {
    'graph': {
        # 'label': 'A Fancy Graph',
        # 'fontsize': '10',
        # 'fontcolor': 'white',
        # 'bgcolor': '#333333',
        # 'rankdir': 'LR',
        # 'rank': 'clusters',
        # 'overlap': 'scalexy',
        # 'splines': 'true',
        # 'nodesep': '1',
    },
    'nodes': {
        # 'fontname': 'Helvetica',
        # 'shape': 'hexagon',
        # 'fontcolor': 'white',
        # 'fontsize': '10',
        # 'color': 'white',
        # 'style': 'filled',
        # 'fillcolor': '#006699',
    },
    'edges': {
        # 'style': 'dashed',
        # 'weight': '0.9',
        # 'color': 'white',
        # 'arrowhead': 'open',
        # 'fontname': 'Courier',
        # 'fontsize': '5',
        # 'fontcolor': 'white',
    }
}

def apply_styles(graph, styles):
    graph.graph_attr.update(
        ('graph' in styles and styles['graph']) or {}
    )
    graph.node_attr.update(
        ('nodes' in styles and styles['nodes']) or {}
    )
    graph.edge_attr.update(
        ('edges' in styles and styles['edges']) or {}
    )
    return graph


class StateMachine(object):
    def __init__(self, name=None):
        self.name = name
        self.init_states = []
        self.final_states = []
        self.events = []
        self.testcases = []
        self.active_cases = []
        self.cases = []
        self.clusters = []
        self.nodes = []
        self.actives = []

        self.f = Digraph('{0}'.format(name), format='svg')
        self.g = Digraph('state_machine', format='svg')

        # self.f.graph_attr['fontsize'] = '20'
        # self.f.node_attr['fontsize'] = '20'
        # self.g.node_attr['fontsize'] = '20'
        #
        # self.f.edge_attr['fontsize'] = '10'
        # self.g.edge_attr['fontsize'] = '10'
        #
        # self.f.graph_attr['nodesep'] = '1'
        # self.f.graph_attr['ranksep'] = '1'
        #
        # self.g.graph_attr['rankdir'] = 'LR'
        # self.f.graph_attr['rankdir'] = 'LR'

    def node(self, node):
        self.f.node(node)
        if node not in self.nodes:
            self.nodes.append(node)

    def edge(self, init_state, final_state, event=''):
        self.f.edge(init_state, final_state, event)

        if init_state not in self.nodes:
                self.nodes.append(init_state)
        if final_state not in self.nodes:
                self.nodes.append(final_state)

        if event not in self.events:
            self.events.append(event)
        if init_state not in self.init_states:
            self.init_states.append(init_state)
        if final_state not in self.final_states:
            self.final_states.append(final_state)

        active_case = [init_state, event, final_state]
        self.actives.append(active_case)
        if active_case not in self.active_cases:
            self.active_cases.append(active_case)

        case = active_case[0:2]
        if case not in self.cases:
            self.cases.append(case)

    def cluster(self, cluster):
        self.f.subgraph(cluster.f)
        self.clusters.append(cluster)

        for event in cluster.events:
            if event not in self.events:
                self.events.append(event)

        for init_state in cluster.init_states:
            if init_state not in self.init_states:
                self.init_states.append(init_state)

        for final_state in cluster.final_states:
            if final_state not in self.final_states:
                self.final_states.append(final_state)

        self.active_cases += cluster.active_cases
        self.cases += cluster.cases

    def view(self):
        self.f.view()

    def csv_saveTrans(self, filename):
        print '==================='
        for tran in self.active_cases:
            print tran[0].encode('utf8'),'->',tran[2].encode('utf8')
        

    def csv_save(self, filename):
        all_cases = map(list, itertools.product(self.init_states, self.events))
        testcases = self.active_cases

        for case in all_cases:
            if case not in self.cases:
                testcases.append([case[0], case[1], '---'])

        self.testcases = testcases

        for testcase in self.testcases:
            print u'状態:{0}, トリガ:{1}, 次の状態:{2}'.format(
                testcase[0], testcase[1], testcase[2]).encode('utf8')

        print len(self.testcases)
        print len(self.events)

        path = os.path.dirname(__file__) + '\\' + filename + '.csv'
        f = open(path, 'w')
        csvWriter = csv.writer(f)

        for testcase in self.testcases:
            for i in range(len(testcase)):
                testcase[i] = testcase[i].encode('utf8')
            csvWriter.writerow(testcase)
        f.close()

    def overview(self):
        active_cases = self.actives
        cases = []
        for active_case in active_cases:
            for cluster in self.clusters:
                if active_case[0] in cluster.nodes:
                    active_case[0] = cluster.name
            for cluster in self.clusters:
                if active_case[2] in cluster.nodes:
                    active_case[2] = cluster.name

            if active_case not in cases:
                cases.append(active_case)
                init_state, event, final_state = active_case
            self.g.edge(init_state, final_state, event)
        self.g.view()


class Cluster(object):
    def __init__(self, name):
        self.name = name
        self.f = Digraph('cluster_{0}'.format(name), format='svg')
        self.f.body.append('label = {0}'.format(name))
        self.f.node_attr['fontsize'] = '10'
        self.f.edge_attr['fontsize'] = '10'
        self.f.graph_attr['rankdir'] = 'LR'
        self.init_states = []
        self.final_states = []
        self.events = []
        self.active_cases = []
        self.nodes = []
        self.cases = []

    def node(self, node):
        self.f.node(node)
        if node not in self.nodes:
            self.nodes.append(node)

    def edge(self, init_state, final_state, event=''):
        self.f.edge(init_state, final_state, event)

        if event not in self.events:
            self.events.append(event)
        if init_state not in self.init_states:
            self.init_states.append(init_state)
        if final_state not in self.final_states:
            self.final_states.append(final_state)

        if init_state not in self.nodes:
                self.nodes.append(init_state)
        if final_state not in self.nodes:
                self.nodes.append(final_state)

        active_case = [init_state, event, final_state]
        if active_case not in self.active_cases:
            self.active_cases.append(active_case)

        case = active_case[0:2]
        if case not in self.cases:
            self.cases.append(case)

    def view(self):
        self.f.view()

    def get_case(self):
        all_cases = map(list, itertools.product(self.init_states, self.events))
        for case in all_cases:
            if case not in self.cases:
                self.testcases.append([case[0], case[1], None])
        for testcase in self.testcases:
            print '状態:{0}, トリガ:{1}, 次の状態:{2}'.format(
                testcase[0], testcase[1].encode('utf8'), testcase[2])

        print len(self.testcases)
        print len(self.events)
