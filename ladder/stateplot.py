# coding: utf-8

# ラダー図からテストケースを見据えた状態遷移図を作成する
# 必要となるもの
#     ユーザが行うイベント
#     ユーザーが把握できる状態



from statemachine import StateMachine
if __name__ == '__main__':
    statemachine = StateMachine('UseCase')
    # usecase.cluster('tool')
    # M input
    statemachine.node(u'S-Con 手動モード')
    statemachine.node(u'S-Con 即停止異常')
    statemachine.node(u'S-Con 異常解除')
    statemachine.node(u'S-Con 原点復帰')
    statemachine.node(u'S-Con 原点復帰')


    # イベント変数の抽出
    # 接点あり，コイルなし
    # SCONからみての入力
    statemachine.edge(u'foo', u'bar', u'L4, 主幹1手動モード')

    statemachine.edge(u'foo', u'bar', u'M14005, 主幹異常解除')
    statemachine.edge(u'foo', u'bar', u'M14010, 即停止異常')
    statemachine.edge(u'foo', u'bar', u'M19990, 個別ダミー用')

    statemachine.edge(u'foo', u'bar', u'X1000, S-Con完了ポジション1')
    statemachine.edge(u'foo', u'bar', u'X1001, S-Con完了ポジション2')
    statemachine.edge(u'foo', u'bar', u'X1002, S-Con完了ポジション4')
    statemachine.edge(u'foo', u'bar', u'X1003, S-Con完了ポジション8')
    statemachine.edge(u'foo', u'bar', u'X1004, S-Con完了ポジション16')
    statemachine.edge(u'foo', u'bar', u'X1005, S-Con完了ポジション32')
    statemachine.edge(u'foo', u'bar', u'X1006, S-Con移動中')
    statemachine.edge(u'foo', u'bar', u'X1007, S-Conゾーン1')
    statemachine.edge(u'foo', u'bar', u'X100A, S-Con原点復帰完了')
    statemachine.edge(u'foo', u'bar', u'X100B, S-Con位置決め完了')
    statemachine.edge(u'foo', u'bar', u'X100D, S-Con非常停止ステータス')
    statemachine.edge(u'foo', u'bar', u'X100E, S-Conアラーム')

    # 状態遷移

    statemachine.edge(u'foo', u'S-Con状態異常', u'M14005, 主幹異常解除')

    # statemachine.edge(u'foo', u'bar', u'USER M00n, S-Con異常停止 ON')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, S-Con異常停止 OFF')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, プロフェッショナルモード ON')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, プロフェッショナルモード OFF')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, 一時停止停止条件 ON')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, 一時停止停止条件 OFF')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, 原点復帰 ON')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, 原点復帰 OFF')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, アブソリュートON')
    # statemachine.edge(u'foo', u'bar', u'USER M00n, アブソリュートOFF')
    
    # 状態遷移
    statemachine.edge(u'foo', u'手動モード', u'USER M14005, 主幹異常解除')


    # X input
    statemachine.edge(u'foo', u'bar', u'USER D1000, S-Con指示レジスタバッファ0->n')

    statemachine.view()
    statemachine.csv_save('aaaa')

