﻿# coding: utf-8
from statemachine import StateMachine
if __name__ == '__main__':

    statemachine = StateMachine('UseCase')
    # usecase.cluster('tool')

    statemachine.edge(u'状態1', u'状態2', u'e1')
    statemachine.edge(u'状態2', u'状態3', u'e2')
    statemachine.edge(u'状態3', u'状態1', u'e3')

    # statemachine.view()
    statemachine.csv_save('aaaa')
